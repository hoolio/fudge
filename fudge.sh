#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.
# https://github.com/hooliowobbits/fudge
# Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# __ ABOUT __
# fudge takes configs (here called toolchains) and makes new images from old
# using packer from hashicorp.  fudge does fancy stuff with said toolchains
#
# See README.md for more details
#
# __ USAGE __
#  call the script with no arguments
#
# __ DEPENDENCIES __
# libstring-random-perl - Perl module for generating random strings
# libdatetime-format-strptime-perl - Perl module to parse and format strp and strf time patterns
# ansible - Configuration management, deployment, and task execution system
# python-glanceclient - Client library for Openstack glance server
# python-openstackclient - OpenStack Command-line Client
# libyaml-tiny-perl
# ..plus various "normal" things like; rename, rm, rsync, sed, screen, grep etc
# .. if it says it's missing, you probably need it :)
#
# __ NOTES__
# if you get "unsupported protocol scheme" you need to source your openrc.sh
#
# __ BUGS!!__
#  BUGS BUGS EVERYWHERE AND NOT A DROP TO DRINK!!
#  'sed -i 's/rstudio.001/rstudio.002/' ./rstudio.002/packer.rstudio.002.json' doesn't work
#      via the script, but if you run it again, it works (so i do it twice); race condition?
#
# __ TO DO NEXT VERSION __
#
# __ TO DO SOMETIME IN THE FUTURE __
# automatically set and unset the public state of an image (of a toolchain)
# record freeform information about a toolchain; what it has installed, the intentions with it etc

#use strict;
use 5.010;
no warnings 'experimental::smartmatch';
use Date::Format;
use Date::Parse;

# strip trailing spaces from incoming arguments (so we can use tab complete)
my $num_args=$#ARGV + 1;
for (my $i=0; $i <= $num_args-1; $i++) {
   #print "arg$i = $i is $ARGV[$i]";
   $ARGV[$i] =~ s/\///g;
   #print " and is now $ARGV[$i]\n";
}

# declare this here to avoid recursion issues
sub sub_get_detail_from_log($ $);

# parse argument and sub arguments
given ($ARGV[0]) {
    when("new") { &sub_new_toolchain($ARGV[1]); }
    when("launch") { &sub_launch_toolchain($ARGV[1]); }
    when("bake") { &sub_bake_from_toolchain($ARGV[1]); }
    when("delete") { &sub_delete_toolchain($ARGV[1]); }
    when("increment") { &sub_increment_toolchain($ARGV[1]); }
    when("list") { &sub_list_toolchains($ARGV[1]); }
    #when("metadata") { &sub_toolchain_image_metadata($ARGV[1]); }
    when("time") { &sub_time_build($ARGV[1]); }
    when("info") { &sub_toolchain_info($ARGV[1]); }
    when("check") { &sub_check_toolchain_config($ARGV[1]); }
    when("update-metadata") {&sub_update_image_metadata($ARGV[1]); }
    #when("copy") { &sub_copy_toolchain($ARGV[1],$ARGV[2]); }
    default { &sub_show_usage(); }
}

sub sub_show_usage() {
    print "usage: fudge.sh new <toolchain>\n";
    print "       fudge.sh increment <toolchain.version>\n";
    print "       fudge.sh delete <toolchain.version>\n";
    print "       fudge.sh bake <toolchain.version>\n";

    # these options hidden from users, may or may not work beware
    #print "       fudge.sh copy <source> <destination>\n";
    print "       fudge.sh list\n";
    #print "       fudge.sh info toolchain\n";
    print "       fudge.sh launch <toolchain.version>\n";
    #print "       fudge.sh time <toolchain>\n";
    #print "       fudge.sh check <toolchain>\n";
    print "       fudge.sh update-metadata <toolchain>\n";

    exit;
}

sub sub_update_image_metadata($) {
  # this takes the content of metadata.bob.001.txt and sends each attribute to glance
  # it's coded to only care about a certain set of attributes, will require more code
  # to increase that set.
  #
  $Data::Dumper::Useqq = 1;
  use Data::Dumper qw(Dumper);
  use YAML::Tiny;  # from libyaml-tiny-perl
  my ($command, $result, @words);

  my $toolchain_name=$_[0];
  my $metadata_file_path="./toolchains/$toolchain_name/metadata.$toolchain_name.yml";
  my $current_uuid = sub_get_detail_from_log($toolchain_name, "uuid"); trim($current_uuid);
  #print Dumper $current_uuid;

  print "Found $current_uuid in $toolchain_name\n";

  if (-e $metadata_file_path){
    print "Reading values from $metadata_file_path\n\n";

    # read metadata file
    my $yaml = YAML::Tiny->read($metadata_file_path); my $config = $yaml->[0];

    # grab config items
    my $local_visibility = $yaml->[0]->{visibility};
    my $local_os_distro = $yaml->[0]->{os_distro};
    my $local_os_version = $yaml->[0]->{os_version};
    my $local_disk_format = $yaml->[0]->{disk_format};
    my $local_container_format = $yaml->[0]->{container_format};
    my $local_min_disk = $yaml->[0]->{min_disk};
    my $local_min_ram = $yaml->[0]->{min_ram};
    my $local_default_user = $yaml->[0]->{default_user};
    my $local_added_packages = $yaml->[0]->{added_packages};
    my $local_description = $yaml->[0]->{description};
    my $local_publisher_name = $yaml->[0]->{publisher_name};
    my $local_publisher_org = $yaml->[0]->{publisher_org};
    my $local_publisher_email = $yaml->[0]->{publisher_email};
    my $local_change_log = $yaml->[0]->{change_log};

    # print local config items
    #print Dumper $local_is_public;
    print "  visibility: $local_visibility\n";
    print "  os_distro: $local_os_distro\n";
    print "  os_version: $local_os_version\n";
    print "  disk_format: $local_disk_format\n";
    print "  container_format: $local_container_format\n";
    print "  min_disk: $local_min_disk\n";
    print "  min_ram: $local_min_ram\n";
    print "  default_user: $local_default_user\n";
    print "  added_packages:  $local_added_packages\n";
    print "  description:  $local_description\n";
    print "  publisher_name:   $local_publisher_name\n";
    print "  publisher_org:   $local_publisher_org\n";
    print "  publisher_email:   $local_publisher_email\n";
    print "  change_log:   $local_change_log\n";

    # check to see we want them uploaded
    print "\n ..Update glance?"; sub_verify_to_proceed();

    # Set properties and attributes
    $command = "openstack image set --property os_distro='$local_os_distro' --property os_version='$local_os_version' --property disk_format='$local_disk_format' --property container_format='$local_container_format' --property default_user='$local_default_user' --property added_packages='$local_added_packages' --property description='$local_description' --property publisher_name='$local_publisher_name' --property publisher_org='$local_publisher_org' --property publisher_email='$local_publisher_email' --property change_log='$local_change_log' --min-disk=$local_min_disk --min-ram=$local_min_ram $current_uuid";

    # Update clance config
    print " ....running '$command'\n";$result = `$command`;

    # set image visibility
    if ($local_visibility eq "public") {
      $command = "openstack image set --public $current_uuid";
    } else {
      $command = "openstack image set --private $current_uuid";
    }
    print " ....running '$command'\n";$result = `$command`;

    # show final glance config
    $command = "glance image-show $current_uuid"; $result = `$command`; print "\n$result\n";

  } else {
    print "\nERROR: Unable to find $metadata_file_path";
  }

}

sub ltrim { my $s = shift; $s =~ s/^\s+//; return $s };

sub rtrim { my $s = shift; $s =~ s/\s+$//; return $s };

sub trim {  my $s = shift; $/ = "\n"; chomp($s); rtrim($s); ltrim($s); return $s };

sub sub_check_toolchain_config($){
  my $toolchain_name=$_[0];
  my $toolchain_packer_json_path="./toolchains/$toolchain_name/packer.$toolchain_name.json";
  my $toolchain_ansible_yml_path="./toolchains/$toolchain_name/ansible.$toolchain_name.yml";

  print "Checking $toolchain_name ansible (yml) and packer (json) files for validity\n";

  # do ansible check of config
  ## $ ansible-playbook -c local -i localhost, --syntax-check toolchains/core.002/ansible.core.002.yml
  my $command1="/usr/bin/ansible-playbook -c local -i localhost, --syntax-check $toolchain_ansible_yml_path";
  print " ..running '$command1'\n";
  my $result1=`$command1 2>&1`; #print "\nresult = $result\n";

  if($result1 =~ /ERROR/){
    print " ..ERROR detected, aborting.  ansible-playbook returned:\n";
    print "\n$result1\n";
    #exit;
  }

  # check packer config
  #./bin/packer validate ./toolchains/blah2.001/packer.blah2.001.json
  #Template validated successfully.
  my $command2="./bin/packer validate $toolchain_packer_json_path";
  print " ..running '$command2'\n";
  my $result2=`$command2 2>&1`; #print "\nresult = $result\n";

  if(not $result2 =~ /Template validated successfully/){
    print " ..ERROR detected, aborting.  packer returned:\n";
    print "\n$result2\n";

    if ($result2 =~ /\* Get \/\: unsupported protocol scheme/) {
      print " ..it looks like you need to source your openrc.sh\n";
    }
    exit;
  }

  # all is ok
  print " ..no errors detected, but do watch the build logs\n";

  #print "\nDEBUG: exiting\n";
  #exit;
}

# this is to remove duplicate elements from an array
sub sub_uniq {
  my %seen;
  grep !$seen{$_}++, @_;
}

sub sub_toolchain_image_metadata(){
  # note this will break when logs exist but don't contain all the details

  my $toolchain_directory="toolchains";
  @toolchains_directory_contents = <./$toolchain_directory/*>;
  print "Parsing toolchains from $toolchain_directory folder:\n";

  # hold toolchain names in an array
  my @toolchain_bare_names; # ie names without their increment/version number
  my @toolchain_names; # ie names WITH their increment/version number

  # this to determine the list of toolchain bare_names, will have duplicates
  foreach $toolchain_path (@toolchains_directory_contents) {
    my ($blah, $toolchain_name) = split(/chains\//,$toolchain_path);
    if ($toolchain_name eq "default") { next; } # ignore the default toolchain
    push @toolchain_names, $toolchain_name; # determine the toolchain name
    my ($bare_name, $increment) = split(/\./,$toolchain_name); # determine the toolchain_bare_name
    push @toolchain_bare_names, $bare_name; # add bare_name to our list of bare names
  }

  # generate a list of unique toolchain names
  my @unique_toolchain_bare_names = sub_uniq(@toolchain_bare_names);
  #print "@unique_toolchain_bare_names\n";

  # for each unique toolchain name, determine the newest iteration
  foreach $unique_toolchain (@unique_toolchain_bare_names) {
    my @versions_of_this_toolchain;
    #find the versions of this toolchain
    foreach $toolchain_name (@toolchain_names) {
     if ($toolchain_name =~ m/\Q$unique_toolchain/) {
        #print " ..found $unique_toolchain in $toolchain_name";
        my ($bare_name, $increment_of_this_toolchain) = split(/\./,$toolchain_name);
        # keep a record of the versions of this toolchain
        push @versions_of_this_toolchain, $increment_of_this_toolchain;
     } # end of if
    } # end of foreach toolchain_name

    # so the last element in @versions_of_this_toolchain will be our newest version
    my $most_recent_version = $versions_of_this_toolchain[$#versions_of_this_toolchain];

    # so finally, iterate through the list of versions again, and show stuff;
    print " ..$unique_toolchain:\n";
    foreach $version_of_this_toolchain (@versions_of_this_toolchain) {
      my $unique_toolchain_reconstructed = "$unique_toolchain"."."."$version_of_this_toolchain";

      #print "     $version_of_this_toolchain ";
      my $uuidomgthisisbad = sub_get_detail_from_log($unique_toolchain_reconstructed, "uuid");
      print "      $unique_toolchain_reconstructed ($uuidomgthisisbad)";

      if (sub_get_detail_from_log($unique_toolchain_reconstructed, "public")) { print " (public)";}

      if ($version_of_this_toolchain eq $most_recent_version) {
        print " (newest)";
      }
      print "\n";
    }

  } # end of foreach unique_toolchain
}

sub sub_list_toolchains(){
  # note this will break when logs exist but don't contain all the details

  my $toolchain_directory="toolchains";
  @toolchains_directory_contents = <./$toolchain_directory/*>;
  print "NOTE: This will not display any toolchains in toolchains.archived.\n";
  print "\nParsing toolchains from $toolchain_directory folder:\n";

  # hold toolchain names in an array
  my @toolchain_bare_names; # ie names without their increment/version number
  my @toolchain_names; # ie names WITH their increment/version number

  # this to determine the list of toolchain bare_names, will have duplicates
  foreach $toolchain_path (@toolchains_directory_contents) {
    my ($blah, $toolchain_name) = split(/chains\//,$toolchain_path);
    # ignore the default toolchain
    if ($toolchain_name eq "default") { next; }
    # determine the toolchain name
    push @toolchain_names, $toolchain_name;
    # determine the toolchain_bare_name
    my ($bare_name, $increment) = split(/\./,$toolchain_name);
    # add bare_name to our list of bare names
    push @toolchain_bare_names, $bare_name;
  }

  # generate a list of unique toolchain names
  my @unique_toolchain_bare_names = sub_uniq(@toolchain_bare_names);
  #print "@unique_toolchain_bare_names\n";

  # for each $unique_toolchain_bare_names, determine the newest iteration
  foreach $unique_toolchain (@unique_toolchain_bare_names) {
    my @versions_of_this_toolchain;
    #find the versions of this toolchain
    foreach $toolchain_name (@toolchain_names) {
     if ($toolchain_name =~ m/\Q$unique_toolchain/) {
        #print " ..found $unique_toolchain in $toolchain_name";
        my ($bare_name, $increment_of_this_toolchain) = split(/\./,$toolchain_name);
        # keep a record of the versions of this toolchain
        push @versions_of_this_toolchain, $increment_of_this_toolchain;
     } # end of if
    } # end of foreach toolchain_name

    # so the last element in @versions_of_this_toolchain will be our newest version
    my $most_recent_version = $versions_of_this_toolchain[$#versions_of_this_toolchain];

    # so finally, iterate through the list of versions again, and show stuff;
    print " ..$unique_toolchain:\n";
    foreach $version_of_this_toolchain (@versions_of_this_toolchain) {
      my $unique_toolchain_reconstructed = "$unique_toolchain"."."."$version_of_this_toolchain";

      #print "     $version_of_this_toolchain ";
      my $uuidomgthisisbad = sub_get_detail_from_log($unique_toolchain_reconstructed, "uuid");
      print "      $unique_toolchain_reconstructed ($uuidomgthisisbad)";

      if (sub_get_detail_from_log($unique_toolchain_reconstructed, "public")) { print " (public)";}

      if ($version_of_this_toolchain eq $most_recent_version) {
        print " (newest)";
      }
      print "\n";
    }

  } # end of foreach unique_toolchain
}

sub sub_get_detail_from_log($ $) {
  my $toolchain_name=$_[0];
  my $detail=$_[1];
  my $packer_log_path="./toolchains/$toolchain_name/packer.$toolchain_name.log";

  # make sure the toolchain exists
  if (not sub_check_for_toolchain($toolchain_name)) {
    # then make sure theres a logfile
    if (-e $packer_log_path) {
      #print " ..found $packer_log_path!\n";

      given ($detail) {
          when("uuid") {
            my $command = "grep 'openstack: An image was created' $packer_log_path";
            #print " ..running '$command'\n";
            my $result = `$command`;

            if ($result =~ m/An image was created/){
              #my ($sshcreds,$blah) = split(/:/,$source);
              my ($blah, $uuid) = split(/created\:\s+/,$result); chomp($uuid);
              #print " ...uuid: $uuid\n";
              return $uuid;
            } else {
              print "\n      ERROR: log exists, but can't find uuid.\n";
              #exit;
            }
          }

          when("name") {
            #openstack: Creating the image: TPAC core.001 1447714698
            my $command = "grep 'openstack: Creating the image:' $packer_log_path";
            #print " ..running '$command'\n";
            my $result = `$command`;

            if ($result =~ m/Creating the image/){
              #my ($sshcreds,$blah) = split(/:/,$source);
              my ($blah, $name) = split(/image\:\s+/,$result); chomp($name);
              #print " ...name: $name\n";
              return $name;
            } else {
              print "\n      ERROR: log exists, but can't find name.\n";
              #exit;
            }
          }

          when("public") {
            # $ glance image-show 7f06fc07-0daa-43d7-8257-da6b6241d35d | grep public
            # | is_public                             | True
            my $image_uuid = sub_get_detail_from_log($toolchain_name, "uuid"); #  oooooh recursion!
            my $image_name = sub_get_detail_from_log($toolchain_name, "name");
            my $command = "glance image-show $image_uuid | grep visibility";
            #print " ..running '$command'\n";
            my $result = `$command`;

            if($result =~ m/No image with a name/){
              print "\n      ERROR: log exists, but can't query glance (no uuid)\n";
            } else {
            if ($result =~ m/public/){
              #print " ...public: yes\n";
              return 1;
            } else {
              #print " ...public: no\n";
              return 0;
            }
            }


          }
      } # end of given/when

    # can't find logfile
    } else {
      print "\nERROR: $toolchain_name exists, but can't find logfile, exiting.\n";
      #exit;
    }
  # cant find toolchain
  } else {
      print "\nERROR: $toolchain_name does not exist, exiting.\n";
      exit;
  }
}

sub sub_launch_toolchain($) {
  # nova boot --key-name NectarKeypair
  # --image c1f4b7bc-a563-4feb-b439-a2e071d861aa
  # --security-groups ssh,ICMP
  # --flavor m2.tiny MyVmIsAwesome
  my $toolchain_name=$_[0];

  print "Parsing logfile and building boot command\n";

  my $toolchain_uuid=sub_get_detail_from_log($toolchain_name, "uuid");
  my $toolchain_full_name=sub_get_detail_from_log($toolchain_name, "name");
  #my $toolchain_public_status=sub_get_detail_from_log($toolchain_name, "public");
  my $foo = new String::Random; my $random_string = $foo->randpattern("CnCn");

  print " ..toolchain name is $toolchain_name\n";
  print " ..image name is '$toolchain_full_name'\n";
  print " ..uuid is $toolchain_uuid\n";
  #print " ..public is $toolchain_public_status\n";

  my $command="nova boot --key-name hoolio --image $toolchain_uuid --security-groups SSH --availability-zone NCI --flavor m2.small 'Fudging $toolchain_name $random_string'";
  print "\n$command\n\n";
  sub_verify_to_proceed();

  my $result=`$command`;     print " ..result = $result\n";

  # we want to determine the instance id from $result.
  # then repeatedly run nova show on that id, and stop repeating when there is an ip
  #my $new_instance_id = 

  ## I'd like this to return just the uuid and a command to check status and ip

  # my $command="grep source_image $toolchain_full_path";
  # my $result=`$command`;     #print " ..result = $result\n";
  # my (@result_array) = split(/\"/,$result);
  # my $uuid=$result_array[3]; chomp($uuid);
}

sub sub_toolchain_info($) {
  my $toolchain_name=$_[0];
  my $packer_log_path="./toolchains/$toolchain_name/packer.$toolchain_name.log";

  print "Getting all details on the toolchain $toolchain_name\n";

  #sub_get_detail_from_log($toolchain_name, "name");
  #sub_get_detail_from_log($toolchain_name, "uuid");
  sub_get_detail_from_log($toolchain_name, "public");
}

sub sub_increment_toolchain($ $) {
    my $original_toolchain=$_[0];
    #my $new_toolchain=$_[1]; # <- this is now calculated below
    my $default_toolchain="default";
    #my $preferred_base_toolchain=$default_toolchain;
    my $preferred_base_toolchain=$original_toolchain;

    if (sub_check_for_toolchain($original_toolchain)) {
        print "\nERROR: $original_toolchain does not exist, exiting\n";
        exit;
      } else {
        # calculate the new_toolchain name by parsing the existing one and incrementing it.
        my ($bare_name, $increment) = split(/\./,$original_toolchain);
        $increment = $increment + 1;
        $increment = sprintf("%03d", $increment);
        my $new_toolchain= $bare_name.".".$increment;

        if (not sub_check_for_toolchain($new_toolchain)) {
            print "\nERROR: $new_toolchain already exists, exiting\n";
            exit;
        }

        print "New $new_toolchain toolchain will use the image created by $original_toolchain\n";
        print " ..and will otherwise be based on the toolchain $preferred_base_toolchain\n";

        my $original_toolchains_resulting_uuid = sub_get_detail_from_log($original_toolchain, "uuid");

        sub_copy_toolchain($preferred_base_toolchain, $new_toolchain);

        sub_use_this_source_image_uuid($new_toolchain, $original_toolchains_resulting_uuid);
      }
}

sub sub_new_toolchain($) {
    my $new_toolchain=$_[0];
    my $default_toolchain="default";

    # new_toolchain must have a .nnn suffix as a version number.
    #  if this doesn't exist we should create it.
    my ($bare_name, $increment) = split(/\./,$new_toolchain);

    if($increment){
      # there is some increment component there, is it ok?
      $increment_len = length $increment;
      if ($increment_len != 3) {
        print "\nERROR: version number $increment not in nnn format\n";
        exit;
      }
    } else {
      # user didn't give a version number, so we make one.
      #print " ..increment undetermined, calculating a new one\n";
      $increment = "001";
      $new_toolchain = $new_toolchain.".".$increment;
    }

    print "Creating new $new_toolchain based on the $default_toolchain toolchain\n";

    sub_copy_toolchain($default_toolchain, $new_toolchain);
}

sub sub_use_this_source_image_uuid($ $) {
    my $toolchain=$_[0];
    my $use_this_uuid=$_[1];
    my $toolchain_full_path="./toolchains/$toolchain/packer.$toolchain.json";

    print "Substituting uuid $use_this_uuid into $toolchain\n";

    # which uuid is being used in $toolchain?
    my $original_uuid = sub_which_uuid_is_being_used_in_toolchain($toolchain);
    print " ..determined $toolchain originally used uuid $original_uuid\n";

    # swap the old uuid over with the new one
    print " ..so i'm now swapping in the new one\n";
    my $command2="/bin/sed -i 's/$original_uuid/$use_this_uuid/' $toolchain_full_path";
    print " ..running $command2\n";
    my $result2=`$command2`;     #print " ..result2 = $result2\n";

    # which uuid is being used in $toolchain?
    my $new_uuid = sub_which_uuid_is_being_used_in_toolchain($toolchain);

    if (not $new_uuid eq $original_uuid) {
      print " ..$toolchain now using the new uuid OK\n";
    } else {
      print "\nERROR: $toolchain not using the new uuid, exiting\n";
      exit;
    }

}

sub sub_which_uuid_is_being_used_in_toolchain($) {
    my $toolchain=$_[0];
    my $toolchain_full_path="./toolchains/$toolchain/packer.$toolchain.json";

    my $command="grep source_image $toolchain_full_path";
    my $result=`$command`;     #print " ..result = $result\n";
    my (@result_array) = split(/\"/,$result);
    my $uuid=$result_array[3]; chomp($uuid);

    return $uuid;
}

sub sub_copy_toolchain($ $) {
    # to do:
    #  set the name of the toolchain in the various templates
    #  rename the actual templates to be constent with the toolchain

    my $source_toolchain=$_[0];
    my $destination_toolchain=$_[1];
    my $source_toolchain_path="toolchains/$source_toolchain";
    my $destination_toolchain_path="toolchains/$destination_toolchain";

    print "Copying $source_toolchain to $destination_toolchain\n";

    if (sub_check_for_toolchain($source_toolchain)) {
        print "\nERROR: $source_toolchain does not exist, exiting\n";
        exit;
    } else {
        if (sub_check_for_toolchain($destination_toolchain)) {
            # first we have to copy the files in
            #my $command1="/usr/bin/rsync -av --exclude *.log --exclude *ansible.common* --exclude *metadata* ./$source_toolchain_path/ ./$destination_toolchain_path/";
            my $command1="/usr/bin/rsync -av --exclude *.log --exclude *ansible.common* ./$source_toolchain_path/ ./$destination_toolchain_path/";

            print " ..running '$command1'\n";
            my $result1=`$command1`;

            # for each of the files in the source toolchain, copy them to the destination toolchain.
            @destination_toolchain_files = <./$destination_toolchain_path/*>;
            foreach $destination_toolchain_file (@destination_toolchain_files) {
                # don't copy the logfile from the source toolchain
                if ($destination_toolchain_file =~ m/.log/) {
                  print " ....ignoring $destination_toolchain_file\n";
                } else {
                  # we need to replace references in those files of the old to the new toolchains
                  my $command3="/bin/sed -i 's/$source_toolchain/$destination_toolchain/' $destination_toolchain_file";
                  print " ..running '$command3'\n";
                  my $result3=`$command3`;
                  sleep 1; # this is here because sed wasn't doing it correctly the first time......
                  $result3=`$command3`;

                  # then we have to rename them to suit the new toolchain name
                  my $command2="/usr/bin/rename 's/$source_toolchain/$destination_toolchain/' $destination_toolchain_file";
                  print " ..running '$command2'\n";
                  my $result2=`$command2`; chomp($result2);

                }
            } # end of foreach file sed foo

            # grep in the destination_toolchain to see if the seds worked
            my $command4="grep -Ri $source_toolchain ./$destination_toolchain_path/ | grep -v .log | wc -l";
            print " ..running '$command4'\n";
            my $result4=`$command4`; #chomp($result4);

            if ($result4 > 0) {
              print "\nERROR:  Detected failure in the sed operation, exiting.  $destination_toolchain is misconfigured.\n";
              exit;
            }

        } else {
            print "\nERROR: $destination_toolchain already exists, exiting.\n";
            exit;
        }
    }
}

sub sub_check_for_toolchain($) {
    my $toolchain=$_[0];
    my $toolchain_path="./toolchains/$toolchain";

    if (-e $toolchain_path) {
        #print " ....(sub_check_for_toolchain) toolchain $toolchain exists!\n";
        return 0;
    } else {
        #print " ....(sub_check_for_toolchain) toolchain $toolchain does not exist\n";
        return 1;
    }
}

sub sub_delete_toolchain($) {
    my $existing_toolchain=$_[0];
    my $existing_toolchain_path="./toolchains/$existing_toolchain";

    print "Initiating deletion of $existing_toolchain (yes really!)\n";

    if (not sub_check_for_toolchain($existing_toolchain)) {
        print " ..toolchain $existing_toolchain found, preparing to delete it\n";

        if (sub_verify_to_proceed()) {
            my $command="rm -rf $existing_toolchain_path";
            print " ..running '$command'\n";
            my $result=`$command`;
        }
    } else {
        print "\nERROR: $existing_toolchain does not exist, exiting\n";
        exit;
    }
}

sub sub_bake_from_toolchain($) {
   my $toolchain_name=$_[0];
   my $toolchain_full_path="./toolchains/$toolchain_name/packer.$toolchain_name.json";
   my $packer_log_path="./toolchains/$toolchain_name/packer.$toolchain_name.log";
   my $screen_log_proper="./toolchains/$toolchain_name/screen.$toolchain_name.log";
   my $screen_log="./screenlog.0";

   # check toolchains for errors, will exit; if errors are found
   sub_check_toolchain_config($toolchain_name);

   print "Running packer build using $toolchain_full_path\n";

   if (not sub_check_for_toolchain($toolchain_name)) {
      # setup packer environment
      $ENV{'PACKER_LOG'} = '1';
      $ENV{'PACKER_LOG_PATH'} = $packer_log_path;

      # delete the old logfiles if they exist
      if (-e $packer_log_path) { `rm $packer_log_path`; }
      if (-e $screen_log_proper) { `rm $screen_log_proper`; }

      # run packer with the supplied template
      # ./bin/packer build templates/template.json
      my $command2="/usr/bin/screen -d -L -m ./bin/packer build $toolchain_full_path";
      print " ..running '$command2'\n";
      my $result2=`$command2`; #print "\nresult = $result\n";

      # some helpful debug foo
      print " ..packer will now build in a detached screen session.\n";
      print " ....view packer log `tail -f $packer_log_path`\n";
      #print "\n";
      #print "NOTE: Test any images you create before relying on them!!\n";

      # move screenlog into toolchain directory
      if (-e $screen_log) { `mv ./screenlog.0 $screen_log_proper`; }
   } else {
      print "\nERROR: Unable to find toolchain $toolchain_name, exiting\n";
      exit;
   }
}

sub sub_verify_to_proceed() {
    use String::Random;
    my $foo = new String::Random;
    #my $random_string = $foo->randpattern("CcCnCn");
    my $random_string = $foo->randpattern("Cn");
    #my $random_string=ASDFASD;

    print " ..enter $random_string to verify:  ";
    chomp(my $user_input = <STDIN>);
    if ($user_input eq $random_string) {
        #print "Verification passed\n";
        return "verified";
    } else {
        print "\nERROR: Verification failed, exiting\n";
        exit;
    }
}

sub sub_time_build($) {
    my $toolchain_name=$_[0];
    my $packer_log_path="./toolchains/$toolchain_name/packer.$toolchain_name.log";

    print "Calculating the build times of $toolchain_name\n";

    # open the logfile, read it into an array
    open $packer_log_path_handle, '<', $packer_log_path or die " ..unable to open file '$packer_log_path' for reading : $!";
    my @packer_log = <$packer_log_path_handle>;
    close $packer_log_path_handle;  # close as soon as possible

    # packer start
    my $line = $packer_log[0];
    my (@elements) = split(/\s+/,$line);
    my $packer_start = $elements[0]." ".$elements[1];
    my $packer_start_sse = str2time($packer_start);

    # build begin
    my @lines = grep /Waiting for state to become\: \[ACTIVE\]/, @packer_log;
    $line = $lines[0];
    (@elements) = split(/\s+/,$line);
    my $build_start = $elements[0]." ".$elements[1];
    my $build_start_sse = str2time($build_start);

    # build active
    @lines = grep /Waiting for state to become\: \[ACTIVE\]/, @packer_log;
    my $number_of_lines = @lines;
    $line = $lines[$number_of_lines-1];
    (@elements) = split(/\s+/,$line);
    my $build_active = $elements[0]." ".$elements[1];
    my $build_active_sse = str2time($build_active);

    # SSH active
    @lines = grep /handshake complete\!/, @packer_log;
    $line = $lines[0];
    (@elements) = split(/\s+/,$line);
    my $ssh_active = $elements[0]." ".$elements[1];
    my $ssh_active_sse = str2time($ssh_active);

    # image creation / snapshot start
    @lines = grep /Waiting for image creation status\: SAVING/, @packer_log;
    $line = $lines[0];
    (@elements) = split(/\s+/,$line);
    my $snap_start = $elements[0]." ".$elements[1];
    my $snap_start_sse = str2time($snap_start);

    # image creation / snapshot end
    @lines = grep /Waiting for image creation status\: SAVING/, @packer_log;
    $number_of_lines = @lines;
    $line = $lines[$number_of_lines-1];
    (@elements) = split(/\s+/,$line);
    my $snap_end = $elements[0]." ".$elements[1];
    my $snap_end_sse = str2time($snap_end);

    # print out the stuff
    print " ..build_start:   $build_start ($build_start_sse) 0 secs\n";

    my $time_delta = $build_start_sse - $packer_start_sse;  my $time_total=$time_delta;
    print " ..packer_start:  $packer_start ($packer_start_sse) delta ".$time_delta."  total ".$time_total."\n";

    $time_delta = $build_active_sse - $packer_start_sse;  $time_total=$time_total+$time_delta;
    print " ..build_active:  $build_active ($build_active_sse) delta ".$time_delta." total ".$time_total."\n";

    $time_delta=$ssh_active_sse-$build_active_sse;  $time_total=$time_total+$time_delta;
    print " ..ssh_active:    $ssh_active ($ssh_active_sse) delta ".$time_delta." total ".$time_total."\n";

    #print " .. in here is some variable build foo\n";

    $time_delta=$snap_start_sse-$build_active_sse;  $time_total=$time_total+$time_delta;
    print " ..snap_start:    $snap_start ($snap_start_sse) delta ".$time_delta." total ".$time_total."\n";

    $time_delta=$snap_end_sse-$snap_start_sse;  $time_total=$time_total+$time_delta;
    print " ..snap_end:      $snap_end ($snap_end_sse) delta ".$time_delta." total ".$time_total."\n";
}
