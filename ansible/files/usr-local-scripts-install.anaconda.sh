#!/bin/bash
echo **DOWNLOADING ANACONDA**
/usr/bin/wget -O /tmp/Anaconda2-4.4.0-Linux-x86_64.sh https://repo.continuum.io/archive/Anaconda2-4.4.0-Linux-x86_64.sh
echo

echo **RUNNING ANACONDA INSTALLER**
chmod +x /tmp/Anaconda2-4.4.0-Linux-x86_64.sh
/tmp/Anaconda2-4.4.0-Linux-x86_64.sh -b
echo

echo **ACTIVATING ANACONDA ENVIRONMENT**
. ~/anaconda2/bin/activate
echo

# echo **INSTALLING SHORTCUT FOR ANACONDA-NAVIGATOR**
# cp /usr/local/scripts/anaconda-navigator.desktop ~/.local/share/applications/anaconda-navigator.desktop
# cp /usr/local/scripts/anaconda-navigator.desktop ~/Desktop/anaconda-navigator.desktop
# chmod +x ~/Desktop/anaconda-navigator.desktop
#
# echo Have a look for a new "Anaconda Navigator" shortcut
# echo
#
# echo **DELETING SHORTCUT FOR THIS SCRIPT**
# echo You can run this install later with /usr/local/scripts/install.anaconda.sh
# rm ~/.local/share/applications/install-anaconda.desktop
# rm ~/Desktop/install-anaconda.desktop
# echo

echo ..All done!!  You can close this window now

sleep 9999
