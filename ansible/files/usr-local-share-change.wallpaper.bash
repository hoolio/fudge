#!/bin/bash
#
# change wallpaper on MATE machine, via ssh or cron

# the default is /usr/share/backgrounds/ubuntu-mate-utopic/Ubuntu-Mate-Radioactive.jpg
#PIC=/usr/share/backgrounds/mate/nature/Dune.jpg
PIC=$1
#echo `ls -l $PIC`

# cron  needs the DBUS_SESSION_BUS_ADDRESS environment variable set
if [ -z "$DBUS_SESSION_BUS_ADDRESS" ] ; then
 TMP=~/.dbus/session-bus
 export $(grep -h DBUS_SESSION_BUS_ADDRESS= $TMP/$(ls -1t $TMP | head -n 1))
fi

# Set a Background Image
gsettings set org.mate.background picture-filename $PIC
