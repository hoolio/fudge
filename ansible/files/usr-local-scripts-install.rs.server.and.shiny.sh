#!/bin/bash
echo **DOWNLOADING RSTUDIO SERVER DEB**
/usr/bin/wget -O /tmp/rstudio-server.deb https://download2.rstudio.org/rstudio-server-1.0.136-amd64.deb
echo

echo **INSTALLING RSTUDIO SERVER**
sudo /usr/bin/gdebi --non-interactive /tmp/rstudio-server.deb
echo

echo **DOWNLOADING RSTUDIO SHINY DEB**
/usr/bin/wget -O /tmp/shiny-server.deb https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.3.838-amd64.deb

echo **INSTALLING RSTUDIO SHINY**
sudo /usr/bin/gdebi --non-interactive /tmp/shiny-server.deb
echo

echo ..All done!!  You can close this window now

sleep 99
