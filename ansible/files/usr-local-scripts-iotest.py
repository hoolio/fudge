#!/usr/bin/python
#
# iotest.sh
# Julius Roberts 2017
# https://github.com/hooliowobbits/scripts/tree/master/system/iotest
#
# Test i/o by doing N write/read tests from/to /dev/zero to/from $target
#
# you may need to install pint:
# $ pip install -r requirements.txt

import sys
import os
import subprocess
import argparse
import pint

DEVNULL = open(os.devnull, 'wb')
ureg = pint.UnitRegistry()

#-----------
# ARGPARSE
#-----------
parser = argparse.ArgumentParser(description='Do N write/read tests from/to /dev/zero to/from $target',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--source', type=str, default='/dev/zero', help='Where to read data from.')
parser.add_argument('dest', type=str, help='Where to write data to.')
parser.add_argument('--count', type=int, default=4, help='Number of test iterations.')
parser.add_argument('--size', type=int, default=150, help='Payload per iteration in MB.')
#parser.add_argument('--verbose', action='store_true', help='Output return from each test.')

# print help if no args given
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

#-----------
# COMMANDS
#-----------
write_test='dd if=%s of=%s/deleteme.dat bs=1M count=%s oflag=direct' % (args.source,args.dest,args.size)
read_test='dd if=%s/deleteme.dat of=/dev/null bs=1M count=%s iflag=direct' % (args.dest,args.size)

#-----------
# DEF TEST
#-----------
def perform_test(test):
  test_command=test + ' 2>&1 | grep copied'
  list_of_speeds_in_MBs = []
  for x in range(0,args.count):
    p = subprocess.Popen(test_command, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL, close_fds=True)
    output = p.stdout.read()

    print ".." + output.rstrip('\n')

  #   output = output.split()
  #   speed = output[9]
  #   unit = output[10]
  #
  #   if unit == 'GB/s':
  #     speed = speed * ureg.meter
  #     # unit reference chart at https://github.com/hgrecco/pint/blob/master/pint/default_en.txt
  #
  #   print '%s' % unit
  #   list_of_speeds_in_MBs.append(speed)
  #
  # print "%s" % list_of_speeds_in_MBs
  #
  # list_of_speeds = map(int, list_of_speeds)
  # average = sum(list_of_speeds) / len(list_of_speeds)
  # print "..average is %s" % average

#-----------
# RUN TESTS
#-----------
print "Doing %s write/read tests of %s MB from/to %s to/from %s/deleteme.dat" % (args.count,args.size,args.source,args.dest)

print "writing with: %s" % write_test
perform_test(write_test)

print "reading with with: %s" % read_test
perform_test(read_test)
