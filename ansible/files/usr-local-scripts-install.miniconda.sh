#!/bin/bash

destination_install_path=$1
destination_install_path=${1?install where? the default is "~/.local/miniconda3"}

#destination_install_path="~/.local/miniconda3"

echo **DOWNLOADING MINICONDA**
/usr/bin/wget -O /tmp/Miniconda3-latest-Linux-x86_64.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
echo

echo **INSTALLING MINICONDA TO $destination_install_path**
chmod +x /tmp/Miniconda3-latest-Linux-x86_64.sh
/tmp/Miniconda3-latest-Linux-x86_64.sh -b -p $destination_install_path
echo

echo **ACTIVATING MINICONDA ENVIRONMENT**
. $destination_install_path/bin/activate
echo

echo **USING CONDA TO INSTALL ANACONDA-NAVIGATOR**
conda install anaconda-navigator jupyter -y
echo

echo **INSTALLING SHORTCUT FOR ANACONDA-NAVIGATOR**
cp /usr/local/scripts/anaconda-navigator.desktop ~/.local/share/applications/anaconda-navigator.desktop
cp /usr/local/scripts/anaconda-navigator.desktop ~/Desktop/anaconda-navigator.desktop
chmod +x ~/.local/share/applications/anaconda-navigator.desktop
chmod +x ~/Desktop/anaconda-navigator.desktop

cp /usr/local/scripts/jupyter.desktop ~/.local/share/applications/jupyter.desktop
cp /usr/local/scripts/jupyter.desktop ~/Desktop/jupyter.desktop
chmod +x ~/.local/share/applications/jupyter.desktop
chmod +x ~/Desktop/jupyter.desktop

echo Have a look for a new "Anaconda Navigator" shortcut
echo

echo **DELETING SHORTCUT FOR THIS SCRIPT**
echo You can run this install later with /usr/local/scripts/install.miniconda.sh
rm ~/.local/share/applications/install-anaconda.desktop
rm ~/Desktop/install-anaconda.desktop
echo

echo **MODIFYING .BASHRC TO SOURCE CONDA ENVIRONMENT**
echo >> ~/.bashrc
echo "# source conda environment" >> ~/.bashrc
echo "echo ..sourcing conda from ~/.local/miniconda3/" >> ~/.bashrc
echo "source ~/.local/miniconda3/bin/activate" >> ~/.bashrc

echo ..All done!!  You can close this window now

sleep 15
