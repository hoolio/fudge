#!/bin/bash
echo **ACTIVATING MINICONDA ENVIRONMENT**
. ~/.local/miniconda3/bin/activate
echo

echo **USING CONDA TO INSTALL ANACONDA-NAVIGATOR**
conda install rstudio -y
echo

#echo **INSTALLING SHORTCUT FOR ANACONDA-NAVIGATOR**
#cp /usr/share/applications/conda.rstudio.desktop ~/Desktp
# cp /usr/local/scripts/anaconda-navigator.desktop ~/Desktop/anaconda-navigator.desktop
# chmod +x ~/Desktop/anaconda-navigator.desktop
#
# echo Have a look for a new "Anaconda Navigator" shortcut
# echo
#
# echo **DELETING SHORTCUT FOR THIS SCRIPT**
# echo You can run this install later with /usr/local/scripts/install.anaconda.sh
# rm ~/.local/share/applications/install-anaconda.desktop
# rm ~/Desktop/install-anaconda.desktop
# echo
#
# echo ..All done!!  You can close this window now
#
# sleep 9999
