#!/bin/bash

export ANSIBLE_NOCOWS=1

cp ~/.ssh/authorized_keys ~
cd ~/fudge/
ln -s ~/fudge/ansible/files/ ~/fudge/ansible/tasks/files
git fetch --all
git reset --hard origin/master
git pull
ansible-playbook -i "localhost," -c local ~/fudge/ansible/tasks/localhost.yml
cp ~/authorized_keys ~/.ssh/
