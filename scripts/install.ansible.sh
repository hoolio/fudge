#!/bin/bash -eux

echo --------------------
echo WHOAMI and LSB_RELEASE
echo --------------------
whoami
echo
lsb_release -a
echo

echo -------------
echo APT LOCK WOES
echo -------------
echo Sleep 30 to allow apt-update to start:
sleep 30
# this thx to https://raw.githubusercontent.com/shadow-robot/sr-build-tools/master/bin/deploy_repo.sh
echo Waiting for apt and dpkg locks to clear:
while (sudo fuser /var/lib/apt/lists/lock >/dev/null 2>&1) || (sudo fuser /var/lib/dpkg/lock >/dev/null 2>&1) do
    echo "..zzzz"
    sleep 4
done
echo Sleep 30 to allow apt-upgrade to start:
sleep 30
# this thx to https://raw.githubusercontent.com/shadow-robot/sr-build-tools/master/bin/deploy_repo.sh
echo Waiting for apt and dpkg locks to clear:
while (sudo fuser /var/lib/apt/lists/lock >/dev/null 2>&1) || (sudo fuser /var/lib/dpkg/lock >/dev/null 2>&1) do
    echo "..zzzz"
    sleep 4
done

echo -------------------
echo INSTALL ANSIBLE ETC
echo -------------------
# echo Updating package lists:
# #apt-add-repository -y ppa:ansible/ansible
# apt-get update > /dev/null 2>&1

echo installing packages:
apt-get remove cowsay
apt-get install -y ansible python-pycurl software-properties-common python-urllib3 python-pyasn1 aptitude
