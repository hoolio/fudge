# TPAC rstudio.012

A curated image for marine focussed users featuring rstudio, anaconda-navigator and more.  
Full list of installed software available in ansible.rstudio.012.yml

## Getting started

This image is designed for use with the NECTAR research cloud, and if this image is still public (they're superseded in time) it will be available to choose from the list of images.  look for TPAC and take it from there.

## What's new?

### Rstudio server and shiny server are no longer installed by default.

This has been done for security reasons.  Some users don't need these services, so by not having them running by default we make your instance less prone to attack.  But never fear, you can install them yourself using:

```
$ sudo /usr/local/scripts/install.rs.server.and.shiny.sh
```

### Anaconda navigator has been added

is now installed by default, and will change your bash prompt to show that the default root environment has been activated

```
(root) ubuntu@fudging-core:~$
```

### QGIS has been removed

Due to [irresolvable packaging errors](https://gis.stackexchange.com/questions/217727/broken-packages-and-unmet-dependency-installation-qgis-postgresql-and-postgis-u) QGIS has not been installed.
