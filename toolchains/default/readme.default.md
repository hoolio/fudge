# TPAC default

A curated image containg basic data science applications such as netcdf, anaconda-navigator and more.  
Full list of installed software available in ansible.default.yml

## Getting started

This image is designed for use with the NECTAR research cloud, and if this image is still public (they're superseded in time) it will be available to choose from the list of images.  look for TPAC and take it from there.

## What's new?

### Stuff

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,

```
Where does it come from?
```
