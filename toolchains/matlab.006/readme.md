# TPAC matlab.006

A curated image containg basic data science applications such as netcdf, anaconda-navigator and more.  
Full list of installed software available in ansible.default.yml

## Matlab licensing

Note that this image is only available for UTAS staff.  Please email helpdesk@tpac.org.au if you would like this image made available to you.

## Getting started

This image is designed for use with the NECTAR research cloud, and if this image is still public (they're superseded in time) it will be available to choose from the list of images.  look for TPAC and take it from there.

## What's new?

### Anaconda navigator has been added

is now installed by default, and will change your bash prompt to show that the default root environment has been activated

```
(root) ubuntu@fudging-core:~$
```

### QGIS has been removed

Due to [irresolvable packaging errors](https://gis.stackexchange.com/questions/217727/broken-packages-and-unmet-dependency-installation-qgis-postgresql-and-postgis-u) QGIS has not been installed.
