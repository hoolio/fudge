# Extras; installing stuff on 14.04
This stuff isn't directly fudge related, but does leverage some of our build scripts

## x2go server
```sudo apt-get install ansible curl python-pycurl && curl -O https://raw.githubusercontent.com/hooliowobbits/packer/master/extras/install.x2go.and.mate.yml && sudo ansible-playbook -i "localhost," -c local install.x2go.and.mate.yml```

## owncloud/AARNET cloudstor client
```sudo apt-get install ansible curl python-pycurl && curl -O https://raw.githubusercontent.com/hooliowobbits/packer/master/extras/install.cloudstor.client.yml && sudo ansible-playbook -i "localhost," -c local install.cloudstor.client.yml```
